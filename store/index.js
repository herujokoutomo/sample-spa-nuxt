import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';
import localStorage from 'localStorage';

const vuexPersist = new VuexPersist({
  key: 'cts-admin',
  storage: localStorage
});

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      token: null
    }),
    mutations: {
      setSession(state, token) {
        state.token = token;
      }
    },
    plugins: [vuexPersist.plugin]
  });
};

export default createStore;
