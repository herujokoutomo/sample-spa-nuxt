import axios from 'axios';
import store from '../store/';

const instance = axios.create({
  baseURL: process.env.baseUrl
});

// interceptors
instance.interceptors.request.use(
  config => {
    let vuex = store();
    if (vuex.state.token !== null && vuex.state.token.length > 0) {
      // Add token to header
      let headers = { Authorization: vuex.state.token };
      config.headers = Object.assign({}, config.headers, headers);
    }
    return config;
  },
  err => {
    return Promise.reject(err);
  }
);

export default instance;
