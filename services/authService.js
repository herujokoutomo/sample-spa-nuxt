import api from './api';

const login = (email, password) => {
  return api.post('/api/v1/admin/auth', { email, password });
};

export default {
  login
};
