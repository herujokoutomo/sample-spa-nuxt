import api from './api';

const getProfile = () => {
  return api.get('/api/v1/admin/profile');
};

export default {
  getProfile
};
