import api from './api';

const getPooling = () => {
  return api.get('/api/v1/pools/active');
};

export default {
  getPooling
};
